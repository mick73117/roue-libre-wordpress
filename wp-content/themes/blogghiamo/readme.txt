=== Blogghiamo ===
Contributors: CrestaProject
Tags: two-columns, right-sidebar, custom-background, custom-menu, custom-colors, sticky-post, featured-images, theme-options, translation-ready, accessibility-ready, blog, news
Requires at least: 4.5
Tested up to: 5.1
Stable tag: 1.7.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Blogghiamo WordPress Theme, Copyright 2018 Rizzo Andrea
Blogghiamo is distributed under the terms of the GNU GPL

== Description ==

Simple and Minimal WordPress Theme ideal for a blog, with two columns, unlimited colors and theme options created by CrestaProject.com. Compatible with Gutenberg.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Blogghiamo includes support for Infinite Scroll in Jetpack.

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2017 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
* FontAwesome (http://fontawesome.io/) Font licensed under SIL OFL 1.1 and Code lisensed under MIT
* "Pro" theme section examples for the customizer (https://github.com/justintadlock/trt-customizer-pro) licensed under the GNU GPL, version 2 or later.
* Smooth Scroll script (https://github.com/galambalazs/smoothscroll-for-websites) licensed under MIT
* Image used in Theme Screenshot (http://picjumbo.com/lonely-road-in-the-middle-of-fields/) Picjumbo Free photos for commercial and personal works

== Changelog ==
= Version 1.0 =
* Initial Release

= Version 1.0.1 =
* Minor bug fixes

= Version 1.1 =
* Updated overlay images
* Fixed sub menu effect
* Minor bug fixes

= Version 1.2 =
* Updated main menu effect
* Fixed rich snippets
* Minor bug fixes

= Version 1.2.1 =
* Updated Theme Options
* Update Languages
* Minor bug fixes

= Version 1.2.2 =
* Accessibility update
* Minor bug fixes

= Version 1.2.3 =
* Accessibility update
* Minor bug fixes

= Version 1.2.4 =
* Minor bug fixes

= Version 1.2.5 =
* Minor bug fixes

= Version 1.2.6 =
* Added compatibility with WordPress 4.1
* Update FontAwesome to 4.3.0 Version
* Minor Bug Fixes

= Version 1.2.7 =
* Added menu for Theme Options in the admin toolbar
* Fixed icon position for sticky posts
* Minor bug fixes

= Version 1.2.8 =
* Added link to email
* Minor bug fixes

= Version 1.2.9 =
* Added VK social button
* Fixed Previous and Next Post translation bug
* Minor bug fixes

= Version 1.3.3 =
* Updated FontAwesome to 4.4.0 Version
* Move the "Theme Options" in "WP Customizer"
* Minor bug fixes

= Version 1.3.4 =
* Updated Email Button
* Minor bug fixes

= Version 1.3.5 =
* Updated FontAwesome to 4.5.0 Version
* Minor bug fixes

= Version 1.3.6 =
* Added two new social buttons
* Fixed input type color

= Version 1.3.7 =
* Added Disqus Support
* Minor bug fixes

= Version 1.3.8 =
* Updated SmoothScroll to 1.4.0 version
* Updated H2 post title
* Minor bug fixes

= Version 1.3.9 =
* Fixed site title and site description heading
* Fixed mobile view
* Improved mobile menu
* Improved RTL mode
* Updated SmoothScroll to 1.4.4 version
* Minor bug fixes

= Version 1.4.0 =
* Updated FontAwesome to 4.6.1 version
* Improved page navigation
* Minor bug fixes

= Version 1.4.1 =
* Updated FontAwesome to 4.6.3 version
* Updated HTML5Shiv to 3.7.3 version
* Minor bug fixes

= Version 1.4.2 =
* Added admin panel
* Minor bug fixes

= Version 1.4.3 =
* Fixed mobile menu for 1024px
* Updated hover effect on featured images
* Minor bug fixes

= Version 1.4.4 =
* Improved RTL mode
* Fixed mobile menu
* Minor bug fixes

= Version 1.4.6 =
* Updated SmoothScroll to 1.4.5 version
* Minor bug fixes

= Version 1.4.7 =
* Updated FontAwesome to 4.7.0 Version
* Updated SmoothScroll to 1.4.5 Version
* Improved custom menu in the sidebar
* Minor bug fixes

= Version 1.4.8 =
* Updated PRO Version button
* Updated SmoothScroll to 1.4.6 Version
* Minor bug fixes

= Version 1.4.9 =
* Minor bug fixes

= Version 1.5.0 =
* Fixed a lot of bugs related to the strings escaping
* Minor bug fixes

= Version 1.5.1 =
* Updated skip-link-focus and navigation scripts
* Updated some scripts in the way to detect a mobile device
* Minor bug fixes

= Version 1.5.2 =
* Minor bug fixes

= Version 1.5.3 =
* Now Blogghiamo is no longer compatible with WordPress 4.3 version or lower
* Added icons to Categories Widget and Archives Widget
* Minor bug fixes

= Version 1.5.4 =
* Minor bug fixes

= Version 1.5.5 =
* Fixed some strings escaping
* Fixed Infinite Scroll with JetPack
* Improved search widget CSS

= Version 1.5.6 =
* Fixed some strings escaping

= Version 1.5.7 =
* Fixed some strings escaping
* Minor bug fixes

= Version 1.5.8 =
* Added Xing social button
* Minor bug fixes

= Version 1.5.9 =
* Added compatibility with WordPress 4.8
* Minor bug fixes

= Version 1.6.0 =
* Minor bug fixes

= Version 1.6.1 =
* Added compatibility with Content Options for Jetpack
* Minor bug fixes

= Version 1.6.2 =
* Improved Javascript file
* Minor bug fixes

= Version 1.6.3 =
* Minor bug fixes

= Version 1.6.4 =
* Minor bug fixes

= Version 1.6.5 =
* Minor bug fixes

= Version 1.6.6 =
* Improved readme.txt file

= Version 1.6.7 =
* Added Telegram social button

= Version 1.6.8 =
* Improved style when sidebar is not active
* Fixed version tag for styles and scripts
* Added Imdb social button
* Minor bug fixes

= Version 1.6.9 =
* Fixed a bug with the main menu on tablet
* Minor bug fixes

= Version 1.7.0 =
* Improved compatibility with Gutenberg and WordPress 5.0
* Updated SmoothScroll
* Minor bug fixes

= Version 1.7.1 =
* Added the possibility to add custom Copyright text
* Updated SmoothScroll to 1.4.9 version
* Minor bug fixes