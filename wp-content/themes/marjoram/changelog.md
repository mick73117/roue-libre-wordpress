###  CHANGELOG Version Numbers: 1.2.3

1. An increment in the major version indicates a break in backward compatibility.
2. An increment in the minor version indicates the addition of new features or a significant change to existing features.
3. An increment in the patch version indicates that minor features have been changed or bugs have been fixed.


### 1.0.2 - February 6, 2019

**Changes:** 

- Updated URLs to Blogging Theme Styles
- Added a $5 discount for upgrade to pro
- Updated the Customizer and About page upgrade/upsell information
- Updated the language .pot translation file


### 1.0.1 - August 30, 2018

**Changes:**

- Fixed the colour setting for the bottom sidebar background not changing
- Renamed the bottom social icon colour settings by removing "Top" from the name.
- Updated the language .pot translation file


### 1.0.0 - May 20, 2018

**Changes:** 

- Theme release as first version

